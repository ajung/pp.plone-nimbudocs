from zope.component import adapter
from ZPublisher.interfaces import IPubEvent,IPubAfterTraversal
from Products.CMFCore.utils import getToolByName
from AccessControl.unauthorized import Unauthorized
from zope.component.hooks import getSite

@adapter(IPubAfterTraversal)
def CORS(event):
    """ Protect anonymous users from access to folder_listing etc. """
    request = event.request
    request.response.setHeader('Access-Control-Allow-Origin', '*')
    request.response.setHeader('Access-Control-Allow-Headers', 'Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With')
    request.response.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST')
