var NIMBUDOCS_URL = 'http://eubeta.nimbudocs.com';
var context_url = $('base').attr('href');

(function worker() {
  var base = $('base').attr('href');
  $.ajax({
    url: base + '/@@get-nimbudocs-info', 
    success: function(data) {
      $('#nimbudocs-control').remove();        
      $('h1.documentFirstHeading').append(data);
    },
    complete: function() {
      // Schedule the next request when the current one's complete
      setTimeout(worker, 5000);
    }
  });
})();

function open_editor() {
    return _open_editor();
}

function open_editor_plone() {
    var options = {
        uiConfig: "++resource++pp.plone.nimbudocs/extensions/uiconfig-plone.json",
    };    
    return _open_editor(options);
}

function _open_editor(custom_options) {
    $('#content-core').hide('fast');
    $('#nimbuOuter').show('fast');

    var options = {
        width: '100%',
        height: '100%',
        collabUrl: context_url + "/@@nimbudocs-collab",
        uiConfig: "++resource++pp.plone.nimbudocs/extensions/uiconfig.json",
        actionMapExtension: "++resource++pp.plone.nimbudocs/extensions/actionMap.ext.json",
        localeExtension: {
            "en-US": "++resource++pp.plone.nimbudocs/extensions/en-US.ext.json",
            "de-DE": "++resource++pp.plone.nimbudocs/extensions/de-DE.ext.json",
            "fr-FR": "++resource++pp.plone.nimbudocs/extensions/fr-FR.ext.json"
        },
        userAgentStylesFromUrl: "++resource++pp.plone.nimbudocs/extensions/requiredStyleTemplates.css",
        onready: function() {
            editor = this;
            initCustomActions(this);
            //initSourceViewAction(this);
            jQuery.ajax({
                    dataType: 'json',
                    url: context_url + '/@@get-nimbudocs-content',
                    success:  function(data) {
                        var content = data['content'];
                        if (Modernizr.localstorage) {
                            var key = 'nimbudocs-' + data['uid'] + new Date().toISOString();
                            localStorage.setItem(key, content);
                        }
                        editor.loadDocument(content);
                        editor.invokeAction('zoom-mode-window-height-client');
                    },
                    error: function() {alert('error')}
            });

        },
        onnotsupported: function() {
            alert("This browser does not meet the requirements of Nimbudocs Editor.");
        },
        "presetcolors": {
            "Crimson": "#DC143C",
            "Dark olive green": "#556B2F",
            "F0FFF0": "#F0FFF0",
        },
        "autocorrect": {
            "enabled": true,
            "correctors": {
                "autohyperlinking": true,
                "sentencestartuppercase": true,
                "cellstartuppercase": true,
                "expressionreplacer": true,
                "wordstartcapslock": true 
            },
            "presetcorrections": {
                "": {
                    "1/4": "¼",
                    "1/2": "½",
                    "3/4": "¾",
                    "(r)": "®",
                    "(c)": "©",
                    "tm": "™",
                    ":-)": "☺",
                    "-->": "→",
                    "<--": "←",
                    "<->": "↔"
                },
                "en-US": {
                    "i.e.": "that is",
                    "e.g.": "for example" 
                },
                "de-DE": {
                    "d.h.": "das heißt",
                    "evtl.": "eventuell" 
                },
                "fr-FR": {
                    "s.o.": "sans objet",
                    "n.a.": "non applicable",
                    "p.": "page" 
                } 
            } 
        },
        "allowdefaultcolors": true,
        "allowpresetcolors": true,
        "allowfreecolors": true,
        widgets: {
            sidebar: {
                alwaysOpen: false
            },
            searchbar: {
                position: "top-right",
                simple: false
            }
        }
    };

    /* Copy over custom options if available */
    if (custom_options != undefined) {
        for (var name in custom_options) {
            options[name] = custom_options[name];
        }
    }
    console.log(options);
    NimbudocsEditor.create("nimbuContainer", 
                           NIMBUDOCS_URL, 
                           options);
}

$(document).ready(function() {

    /* Load Nimbudoc Editor scripts */
    var scripts = ["http://eubeta.nimbudocs.com/samples/lib/jquery.min.js",
                   "http://eubeta.nimbudocs.com/samples/lib/jquery-ui.min.js",
                   "http://eubeta.nimbudocs.com/nimbudocseditor.js",
                   "++resource++pp.plone.nimbudocs/extensions/custom-actions.js",
                    ]

    for (i=0; i<scripts.length; i++) {
        var script = scripts[i];
        var s = document.createElement("script");
        s.type = "text/javascript";
        s.src = script;
        $("head").append(s);
    }

    /* Inject container div for editor */
    $('h1.documentFirstHeading').append('<div id="nimbuOuter" style="display: none; height: 600px; width: 100%"><div id="nimbuMessage"></div><div id="nimbuContainer"></div></div>');
});
