function setCollaborationUrl(url) {
    var data = {url: url};
    jQuery.post(context_url + '/@@set-nimbudocs-collaboration-url', data, function() {
            console.log('collab updated: ' + url);
        }
    );
}

function initCustomActions(editor){

    editor.getAction("plone-save").addInvocationListener(function(event) {

        var context_url = $('base').attr('href');
        var html = editor.getDocument();
        var data = {html: html,
                    pdf_url: editor.getPdfUrl(true)
                   };
        
        $('#nimbuContainer').hide('slow');
        $('#nimbuOuter').css('height: 30px');
        $('#nimbuMessage').removeClass().addClass('info').text('Saving...please wait');

        jQuery.ajax({
            type: 'POST',
            url: context_url + '/@@set-nimbudocs-content', 
            data: data, 
            success: function() {
                window.location.reload()
            },
            error: function() {
                $('#nimbuContainer').show();
                $('#nimbuMessage').removeClass().addClass('error').text('Error saving content');
            }
        });
        event.preventDefault();
    });

    editor.getAction("toggle-collab-mode").addInvocationListener(function(event) {
        var action =  editor.getAction("toggle-collab-mode");
        if (action.getSelectedState() === false) {
            setCollaborationUrl(editor.getCollabUrl())
        } else {
            setCollaborationUrl('')
        }
    });
}
