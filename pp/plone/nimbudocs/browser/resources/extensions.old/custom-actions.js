function setCollaborationUrl(url) {
    var data = {url: url};
    jQuery.post('@@set-nimbudocs-collaboration-url', data, function() {
            console.log('collab updated ' + url);
        }
    );
}

function initCustomActions(editor){

    var ploneSave = editor.getAction("plone-save");
    if (ploneSave !== null) {
        ploneSave.invoke = function() {
            var html = editor.getDocument();
            var data = {html: html};
            var context_url = $('base').attr('href');
            jQuery.post(context_url + '/@@set-nimbudocs-content', data, function() {
                    alert('saved');
                    jQuery('#nimbuContainer').hide();
                }
            );
        };
    }

    var collabAction = editor.getAction("toggle-collab-mode");
    collabAction.addEventListener(Action.prototype.PROPERTY_SELECTED_STATE, function() {
        if (collabAction.getSelectedState() === true) {
            setCollaborationUrl(editor.getCollaborationUrl())
        } else if (collabAction.getSelectedState() === false) {
            setCollaborationUrl('')
        }
    });

    var sourceViewDialog = editor.getAction("source-view-dialog");
    if (sourceViewDialog !== null) {
        sourceViewDialog.invoke = function() {
            // Make sure that the user can not type in the editor while the dialog is opened.
            editor.setLocked(true);
            var dialogId = "sourceViewDialog";
            
            // Create the dialog html
            var dialog = jQuery("<div></div>");
            dialog.attr("id", dialogId);
            dialog.attr("title", editor.localize("L_SOURCE_VIEW"));
            
            var textArea = jQuery("<textarea></textarea>");
            textArea.css({
                "resize"        : "none",
                "padding"       : "0",
                "border"        : "none",
                "font-family"   : "Courier, monospace",
                "font-size"     : "9pt",
                "width"         : "100%",
                "height"        : "400px"
            });
            dialog.append(textArea);
            
            // Insert the dialog
            jQuery("body").append(dialog);
            
            // Get the source of the opened document 
            var source = editor.getDocument();
            textArea.val(source);
            
            dialog.dialog({
                appendTo: "#" + editor.getId(),
                resizable: false,
                width: 800,
                modal: true,
                buttons: [
                    {
                        text: editor.localize("L_CANCEL_BUTTON"),
                        click: function() {
                            jQuery(this).dialog("close");
                        }
                    },
                    {
                        text: editor.localize("L_APPLY_BUTTON"),
                        click: function() {
                            // Get the edited source
                            var newSource = textArea.val();
                            
                            if (newSource !== "" && newSource !== undefined) {
                                // Load the edited source as a new document
                                editor.loadDocument(newSource, editor.getDocumentBaseUrl());
                            }
                            jQuery(this).dialog("close");
                        }
                    }
                ],
                close: function() {
                    jQuery(this).dialog("destroy");
                    dialog.remove();
                    editor.setLocked(false);
                }
            });
        };
    }
}
