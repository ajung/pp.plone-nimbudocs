
import json
import base64
import uuid
import hashlib
from Products.Five.browser import BrowserView
from Products.CMFCore import permissions
from tidylib import tidy_document
from zope.annotation.interfaces import IAnnotations
from zope.pagetemplate.pagetemplatefile import PageTemplateFile
import lxml.html 
from plone import api
import requests
from urlparse import urlparse, parse_qs

ANNOTATION_COLLAB_URL = 'pp.plone.nimbudocs:collab-url'
ANNOTATION_PDF = 'pp.plone.nimbudocs:pdf'

class NimbudocsEditor(BrowserView):

    def getContent(self):
        """ Return HTML body of the content stored in Plone """
        html = self.context.getRawText()
        options = {'input-encoding': 'utf8',
                   'output-encoding': 'utf8'}
        document, errors = tidy_document(html, options)
        root = lxml.html.fromstring(unicode(document, 'utf-8'))
        html_out = lxml.html.tostring(root.xpath('//body')[0], encoding=unicode)
        pt = PageTemplateFile('nimbudocs-content.pt')
        content = pt.pt_render({'context': self.context,
                                'content': html_out})
        data = dict(content=content,
                    url=self.context.absolute_url(),
                    uid=self.context.UID())
        self.request.response.write(json.dumps(data))

    def setContent(self):
        """ Store HTML from Nimbudocs editor and  store the generated PDF.  """

        html = self.request.form['html']
        pdf_url = self.request['pdf_url']

        # disable collaboration first (since the primary editor will be terminated)
        self.setCollaborationUrl('')

        # retrieve PDF from Nimbusdoc editor backend first
        # and store it as an annotation
        result = requests.get(pdf_url)
        annotations = IAnnotations(self.context)
        annotations[ANNOTATION_PDF] = result.content

        # now process HTML before storing it in Plone 
        root = lxml.html.fromstring(html)

        # store inline images
        for img in root.xpath('//img'):
            src = img.attrib['src']
            if src.startswith('data:image/'):
                ident, img_data = src.split(',', 1)
                img_data = base64.decodestring(img_data)
                img_hash = hashlib.sha1(img_data).hexdigest()
                if not img_hash in parent.objectIds():
                    # image does not exist (based on the hash)
                    parent = self.context.aq_parent
                    parent.invokeFactory('Image', id=img_hash)
                    image = parent[img_hash]
                    image.setImage(img_data)
                    image.reindexObject()
                    img_uuid = image.UID()
                else:
                    # image exists
                    image = parent[img_hash]
                    img_uuid = image.UID()
                src = 'resolveuid/{}'.format(img_uuid)
                img.attrib['src'] = src

        nodes = root.xpath('//body')
        body = nodes[0]
        body = (body.text or '') + ''.join(map(lxml.html.tostring, body))
        self.context.setText(body)
        self.request.response.setStatus(200)
        self.request.response.write('OK')

    def setCollaborationUrl(self, url=None):
        annotations = IAnnotations(self.context)
        annotations['nimbudoc-collaboration-url'] = (url or None)
        self.request.response.setStatus(200)
        self.request.response.write('OK')

    def getCollaborationId(self):
        """ Extract collaboration ID from collaboration URL """
        annotations = IAnnotations(self.context)
        collab_url = annotations.get(ANNOTATION_COLLAB_URL)
        if not collab_url:
            return
        qs = urlparse(collab_url).query
        qs_data = parse_qs(qs)
        return qs_data['collabid'][0]

    def getCollaborationUsername(self):
        """ Return current user name """
        user = api.user.get_current()
        return dict(username=user.getUserName(),
                    fullname=user.getProperty('fullname'))

    def getInfo(self):
        """ Get HTML panel for Nimbudocs editor integration """

        # permission check
        if not api.user.get_current().has_permission(permissions.ModifyPortalContent, self.context):
            return
                                    
        annotations = IAnnotations(self.context)
        collab_url = annotations.get(ANNOTATION_COLLAB_URL)
        have_pdf = annotations.get(ANNOTATION_PDF)
        html = '<div id="nimbudocs-control">'
        html += '<button onclick="open_editor()" id="edit-inline">Edit in Nimbudocs Editor</button>'
        html += '&nbsp;&nbsp;<button onclick="open_editor_plone()" id="edit-inline">Edit in Nimbudocs Editor (Plone, minimal)</button>'
        if collab_url:
            html += '&nbsp;&nbsp;<a href="{}"><button>Open document in collaboration mode</button></a>'.format(collab_url)
        if have_pdf:
            html += '&nbsp;&nbsp;<a href="{}/download-pdf"><button>Download PDF</button></a>'.format(self.context.absolute_url())
        html += '</div>'
        return html

    def downloadPDF(self):
        """ Download stored PDF """

        annotations = IAnnotations(self.context)
        data = annotations[ANNOTATION_PDF]
        self.request.response.setHeader('content-type', 'application/pdf')
        self.request.response.setHeader('content-disposition', 'attachment; filename={}.pdf'.format(self.context.getId()))
        self.request.response.setHeader('content-length', str(len(data)))
        self.request.response.write(data)
