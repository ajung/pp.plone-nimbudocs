.. Produce & Publish Plone Client Connector documentation master file, created by
   sphinx-quickstart on Sun Nov 13 15:03:42 2011.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Produce & Publish Plone Client Connector
========================================

The Produce & Publish Plone Client connector integrates the Plone
CMS with the Produce & Publishing platform and supports the
generation of PDF (requires other external PDF converters).

Documentation
-------------

Primary documentation: http://docs.produce-and-publish.com

Produce & Publish website: http://www.produce-and-publish.com

Source code
-----------
See https://bitbucket.org/ajung/pp.client.plone

Bugtracker
----------
See https://bitbucket.org/ajung/pp.client.plone/issues

Licence
-------
Published under the GNU Public Licence Version 2 (GPL 2)

Author
------
| ZOPYX Limited
| Hundskapfklinge 33
| D-72074 Tuebingen, Germany
| info@zopyx.com
| www.zopyx.com



Contents:

.. toctree::
   :maxdepth: 2

   installation.rst
   resource-directories.rst
   content-types.rst
   integration-ploneformgen.rst
   HISTORY.rst

Indices and tables
==================

/ :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

