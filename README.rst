pp.client.plone- Produce & Publish Plone Client Connector
=========================================================

``pp.core`` integrated the Plone CMS with the Produce & Publish.

Requirements
------------

- Plone 4.3 (Plone 4.0 - 4.2 untested)

Source code
-----------

https://bitbucket.org/ajung/pp.client.plone

Bug tracker
-----------

https://bitbucket.org/ajung/pp.client.plone/issues

Support
-------

Support for Produce & Publish Server is currently only available on a project
basis.

License
-------
``pp.core`` is published under the GNU Public License V2 (GPL 2).

Contact
-------

| ZOPYX Limited
| Hundskapfklinge 33
| D-72074 Tuebingen, Germany
| info@zopyx.com
| www.zopyx.com
