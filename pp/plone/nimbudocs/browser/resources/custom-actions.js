function initCustomActions(editor){
    var newDocumentOverride = editor.getAction("new-document");
    if (newDocumentOverride !== null) {
        newDocumentOverride.addInvocationListener(function(e) {
            var response = jQuery.get("source/template.html", function(data){
                editor.loadDocument(data);
            });
            
            e.preventDefault();
        });
    }
}