from setuptools import setup, find_packages
import os

version = '0.1.0'

setup(name='pp.plone-nimbudocs',
      version=version,
      description="Produce & Publish Nimbudocs Editor integration",
      long_description=open("README.rst").read() + "\n" +
                       open(os.path.join("docs", "source", "HISTORY.rst")).read(),
      # Get more strings from http://www.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Plone",
        "Framework :: Plone :: 4.3",
        "Framework :: Zope2",
        "Topic :: Software Development :: Libraries :: Python Modules",
        ],
      keywords='PDF Plone Python EBook EPUB',
      author='Andreas Jung',
      author_email='info@zopyx.com',
      url='http://pypi.python.org/pypi/pp.plone-nimbudocs',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['pp', 'pp.plone'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'BeautifulSoup',
          'lxml',
          'plone.api',
          'pytidylib',
          'archetypes.schemaextender',
          'z3c.jbot',
          'unittest2'
          # -*- Extra requirements: -*-
      ],
      tests_require=['zope.testing'],
      entry_points="""
      [z3c.autoinclude.plugin]
      target = plone
      """,
      )

